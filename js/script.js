/* Author:
 sportsdigita 2013
 */
var dev = window.location.href.slice(window.location.href.indexOf("?") + 1) === "dev";

sound = (dev ?  0 : 1);

var _page = '';

$(document).ready(function() {
    $("#site_prelaod").load("preload.html");
    $("#site_popups").load("popups.html");


    $("#site_navigation")
        .load("navigation.html", function(response, status, xhr){
            if (status == "success") {
                nav.initialize();
                nav.navigate();
              }
        })
    ;


    var Navigation = function() {
        var module = this
            , pages = $(".page")
            , mainNavItem =  $("#site_navigation").find('.nav-item')
        ;

        this.initialize = function() {
            pages = $(".page");
            mainNavItem =  $("#site_navigation").find('.nav-item');
            window.onhashchange = module.navigate;
            pages.hide();
        }

        this.navigate = function() {
            var page = location.hash;

            if (!page) {
                location.hash = '';
                mainNavItem.first().addClass("active");
                return ;
            }

            page = page.replace('#', '');
            _page = page;

            if(page != undefined && page != ''){
                $("#site_navigation").find(".active").removeClass('active');
                $("#site_navigation").find('[data-page='+page+']').addClass("active");
                page = $('#content .' + page + '-page');

            }

            if (page.size()) {
                pages.not(page).hide();
                page.show();
                if(ga){
                    var subPage = ''
                    if(page.data("current").length){
                        subPage = "_" + page.data("current");
                        $("#"+page.data("current")).show();
                    }
                    ga('send', 'pageview', { 'page': location.pathname + location.search  + '#' + _page + subPage});
                }
            }
        }
    }
    var nav = new Navigation();



    /************ Setup Menus ************/
    var setUpNav = (function () {
        var mainNavItem = $('.nav-item');
        var pages = $(".page");
        pages.hide();

        var openPage = function (whichPage) {
            pages.each(function () {
                var self = $(this);
                if (self.hasClass(whichPage)) {
                    self.fadeIn(411);
                    if(ga){
                        ga('send', 'pageview', { 'page': location.pathname + location.search  + '#' + whichPage + "_" + self.data("current")});
                    }
                } else {
                    self.hide();
                }
            });
        }

        mainNavItem.each(function(index) {
            $(this).click(function(){
                mainNavItem.removeClass('active');
                $(this).addClass('active');
                openPage($(this).data("page") + "-page");
            });
        });

        mainNavItem.first().trigger("click");
    });

    var playoffNav = (function () {
        var navItem = $('.playoff-nav-item');
        var pages = $(".playnav-page");
        pages.hide();
        pages.first().fadeIn(1000);

        var openPage = function (whichPage) {
            pages.each(function () {
                if ($(this).hasClass(whichPage)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }

        navItem.each(function(index) {

            $(this).click(function(){
                navItem.removeClass('active');
                $(this).addClass('active');
                openPage($(this).data("page") + "-page");
            });
        });
    });

    /* skip video when hit esc */
    $(document).keyup(function(e) {
        if (e.keyCode == 27) { $('.skip-intro').trigger('click'); }   // esc
    });
    /************ Setup Audio/Video ************/
    var playVideo = true;

    function addSound() {
        /*if(navigator.userAgent.match(/MSIE\s(8.0)/)){
            document.getElementById('background_snd').play();
        }
        else{
            document.getElementById('background_audio').play();
        }
        sound = 1;*/
    }

    function removeSound() {
        if(navigator.userAgent.match(/MSIE\s(8.0)/)){
            document.getElementById('background_snd').pause();
        }
        else{
            document.getElementById('background_audio').pause();
        }
        sound = 0;
    }

    /*function addWoosh() {
        return;
        if(navigator.userAgent.match(/MSIE\s(8.0)/)){
            document.getElementById('woosh_snd').play();
        }
        else{
            document.getElementById('woosh_audio').play();
        }
        sound = 1;
    }

    function removeWoosh() {
        if(navigator.userAgent.match(/MSIE\s(8.0)/)){
            document.getElementById('woosh_snd').pause();
        }
        else{
            document.getElementById('woosh_audio').pause();
        }
        sound = 0;
    }*/

    /************ Setup Audio/Video ************/
    var playVideo = true;
    $('body').find('.jwplayer').each(function() {
        var self = $(this);
        var current = self.attr('id');
        var video = self.data("video");
        var config = {
            events: {
                onComplete: function(){
                    $('.skip-intro').triggerHandler('click');
                }
            },
            image: (video.length ? '../video/' +video+ '.jpg': ''),
            autoplay: 'false',
            width: '883',
            height: '492',
            volume: '100',
            'controlbar.position': "false", // over
            // 'controlbar.idlehide': "true",
            bufferlength: '15',
            backcolor: 'black',
            wmode : 'transparent',
            modes: [

                { type: 'flash', src: '../jwplayer/player.swf' },
                { type: 'html5' }

            ],
            file: "http://www.youtube.com/watch?v=aKZulTCCzGY"
            /*levels: [
             {file: ((dev || window.location.hostname.search(".local")!==-1) ? '../video/' : 'https://s3-us-west-2.amazonaws.com/ebrochure/stars/') +video+'.mp4'},
             {file: ((dev || window.location.hostname.search(".local")!==-1) ? '../video/' : 'https://s3-us-west-2.amazonaws.com/ebrochure/stars/') +video+ '.webm'}
             ]*/
        }

        jwplayer(current).setup(config);
    });

    /************ Load the main site ************/
    var quickHome = (function() {
        var menuQuick = $('#menu');
        var navQuick = $('.nav');
        var bgCount = 1;
        navQuick.show();
        //setUpNav();
        playoffNav();
        $('.intro-logo').hide();
        $('.players').hide();
        $('.welcome').show();
        $('.bg-1').css('opacity', 1);
        $('.body-bg').css({'opacity': '1'});
        $('.play-vid').show();
        menuQuick.animate({'marginTop': '0px'}, 1299);
        $('.renew-box, .upgrade-box').show();
        $('.buy-button').show();
        $('.invoice').show();
        $('.hide-loader').show();
        $('.contact-rep').fadeIn(800);
    });

    var goHome = (function() {
        var menu = $('#menu');
        var nav = $('.nav');
        var backgrounds = $("[class^='bg-']");
        var bgCount = backgrounds.size();
        nav.hide();
        $('.intro-logo').hide();
        $('.players').hide();
        $('.bg-1').css('opacity', 1);
        $('.welcome').show();
        $('.body-bg').animate({'opacity': '1'}, 800);
        $('.play-vid').fadeIn(800);
        $('.hide-loader').show();
        $('.contact-rep').fadeIn(800);
        menu.delay(500).animate({'marginTop': '0px'}, 1299, function() {
            nav.fadeIn();
            $('.right-side-tabs-contain').animate({'right': '-381'}, 200, function() {
                playoffNav();
                $('.renew-box, .upgrade-box').fadeIn(2000);
            });
            $('.buy-button').fadeIn();
            $('.invoice').fadeIn();
        });
        var bgrotator = function(){
            setInterval(function() {
                if (bgCount == backgrounds.size()){
                    backgrounds.last().animate({'opacity': '0'}, 1500);
                    backgrounds.first().animate({'opacity': '1'}, 1500);
                    bgCount = 1;
                }
                else{
                    bgCount++;
                    $('.bg-'+(bgCount-1)).animate({'opacity': '0'}, 1500);
                    $('.bg-'+bgCount).animate({'opacity': '1'}, 1500);
                }

            }, 5000);
        }
        var bgrotator2 = function(){
            setInterval(function() {
                if($('.thanku').parent().parent().hasClass('active')){
                    $('.body-bg div').css({"opacity": "0"});
                    $('.tab6').css({"opacity": "1"});
                    return false;
                }
            }, 1000);
        }
        bgrotator();
        bgrotator2();
        $('body').addClass('pad90');
    });

    $('.home').click(function () {
        var backgrounds = $("[class^='bg-']");
        $('.tab2, .tab3, .tab4, .tab5, .tab6').animate({'opacity': '0'}, 1500);
        backgrounds.animate({'opacity': '0'}, 0);
        backgrounds.first().animate({'opacity': '1'}, 500);
    });
    if (window.location.href.match(/\#/)) {
        quickHome();
    }
    /************ Intro ************/
    var introVid = (function() {
        $('.players').fadeOut(2000);
        $('.video-bg').fadeIn(2800);
        $('.intro-video').animate({'margin-top': '-420px', 'opacity': '1'}, 2500, "swing");
        $('.video-popup').animate({'opacity': '1'}, 7800);
        try {
            document.createEvent("TouchEvent");
        } catch (e) {
            jwplayer('intro-video').play(true);
        }
    });
    var endVideo = (function() {
        jwplayer('intro-video').stop();
        $('.video-bg').fadeOut();
        $('.intro-video').fadeOut();
        goHome();
    });
    $('.play-vid').on('click', function(){
        removeSound();
        $('.popup').hide();
        $('#renew-complete').hide();
        $('.intro-video').css({'margin-top': '-420px'}).fadeIn(800);
        $('.video-popup').css({'opacity': '1'});
        $('.personal-catch').fadeIn(800);
        jwplayer('intro-video').play(true).seek('0');
    });
    $('a .newb').on('click', function(){
        removeSound();
        $('.popup').hide();
        $('#renew-complete').hide();
        $('.popup.newb').fadeIn(800);
        $('.personal-catch2').fadeIn(800);
        $('.popup-contain').fadeIn(800);
        $('#renew').fadeOut('fast');
        jwplayer('newb').play(true).seek('0');
    });
    $('a .tom').on('click', function(){
        removeSound();
        $('.popup').hide();
        $('#renew-complete').hide();
        $('.popup.tom').fadeIn(800);
        $('.personal-catch2').fadeIn(800);
        $('.popup-contain').fadeIn(800);
        $('#renew').fadeOut('fast');
        jwplayer('tomg').play(true).seek('0');
    });
    $('a .thanku').on('click', function(){
        removeSound();
        $('.popup').hide();
        $('#renew-complete').hide();
        $('.popup.thanku').fadeIn(800);
        $('.personal-catch2').fadeIn(800);
        $('.popup-contain').fadeIn(800);
        jwplayer('thankug').play(true).seek('5');
        $('#renew').fadeOut('fast');
    });
    (function () {
        var menu = $('.menu');
        $('#header').css("margin-top", "-227px");
        menu.css("width", "0%");
        menu.css("left", "50%");
        $('#wrapper').css("visibility", "visible");
        lightsHide = $('.players img, .intro-logo div');
        lightsHide.hide();
        $('.intro-logo').show();
        $('.loader-one').show();
        if (!dev && !window.location.href.match(/\#/)) {
            setTimeout(function() {
                $('.loader-two').fadeIn(400);
                setTimeout(function() {
                    $('.loader-three').fadeIn(400);
                    setTimeout(function() {

                                if (!navigator.userAgent.match(/iPad/i)) {
                                    setTimeout(function() {
                                        jwplayer('intro-video').play(true);
                                    }, 3000);
                                }
                                introVid();
                                $('.intro-logo').fadeOut();
                            }, 1080);  // introvid

                }, 1000); // Lader 3
            }, 1000); // Loader 2
        } else {
            quickHome();
        }
    }());

    $('.skip-intro').click(function(){
        endVideo();
        $('.personal-catch').fadeOut(800);
        $('.personal-catch2').fadeOut(800);
        $('.popup-contain').fadeOut(800);
    });

    /************ Countdown Clock ************/
    function getTimeUntil(date) {
        var diff = new Date(date).getTime() - new Date().getTime();
        var cd = 24 * 60 * 60 * 1000,
            ch = 60 * 60 * 1000;
        var days = '' + Math.floor (diff / cd);
        var hours = '0' + Math.floor( (diff - days * cd) / ch);
        var minutes = '0' + Math.floor( (diff - days * cd - hours * ch) / 60000);
        var seconds = '0' + Math.floor( (diff - days * cd - hours * ch - minutes * 60000) / 1000);
        return {
            "days": days,
            "hours": hours.slice(-2),
            "minutes": minutes.slice(-2),
            "seconds": seconds.slice(-2)
        };
    }
    var diff = getTimeUntil("March 28, 2014 23:59:59");  //  2014-03-29 T03:24:00
    $(".scoreboard .days").html(diff.days);
    $(".scoreboard .hours").html(diff.hours);
    $(".scoreboard .minutes").html(diff.minutes);
    $(".scoreboard .seconds").html(diff.seconds);
    var seconds;
    setInterval(function() {
        var diff = getTimeUntil("March 28, 2014 23:59:59"); // 2014-03-29 T03:24:00
        $(".scoreboard .days").html(diff.days);
        $(".scoreboard .hours").html(diff.hours);
        $(".scoreboard .minutes").html(diff.minutes);
        $(".scoreboard .seconds").html(diff.seconds);
    }, 10);

    /************ Right Side Tabs ************/
    $('.jersey-link').click(function() {
        if ($('.personalized-jersey').css('right') == '0px')
        {
            $('.personalized-jersey').delay(50).css('z-index','9000');
            $('.personalized-jersey').delay(50).animate({'right': '381'}, 150);
        } else {
            $('.personalized-jersey').delay(50).animate({'right': '0'}, 150);
            $('.personalized-jersey').delay(50).css('z-index','0');
        }
    });
    $('#contact-rep-trigger').click(function() {
        var self = $(this).parent()
        ;

        $('#ejersey-trigger').parent().removeClass('active');
        if(!self.hasClass('active')){
            self.addClass('active');
        } else {
            self.removeClass('active');
        }

    });

    $('#ejersey-trigger').click(function() {
        var self = $(this).parent()
        ;
        $('#contact-rep-trigger').parent().removeClass('active');

        if(!self.hasClass('active')){
            self.addClass('active');
            self.find('input').val('');
            setTimeout(function() {
                self.find('input').first().focus();
            }, 400);
        } else {
            self.removeClass('active');
        }


    });

//************ Personal Jersey Generator ************
    var personal = (function () {
        var frame = $('.personal-frame');
        var closebg = $('.personal-catch, .personal-catch2, #wrapper .popup-contain .video-popup.popup.tom, #wrapper .popup-contain .video-popup.popup.thanku');
        var launch = $('#personal-btn');
        var shareLaunch = $('#personal-btn-share');
        var img = $("#person-img");
        var name = $("#personal-name");
        var number = $("#personal-number");
        var shareName = $('#personal-name-share');
        var shareNumber = $('#personal-number-share');
        var siteUrl = window.location.href;
        var url = 'http://' + siteUrl.split('/').slice(2,3) + '/';;
        number.focusin(function() {
            number.val('');
        });
        name.focusin(function() {
            name.val('');
        });
        shareNumber.focusin(function() {
            shareNumber.val('');
        });
        shareName.focusin(function() {
            shareName.val('');
        });
        launch.click(function() {
            var personName = name.val();
            var personNumber = number.val();
            console.log(personName.length, personNumber.length);

            if(personNumber.length === 0 || personName.length === 0) {
                if(personNumber.length === 0){
                    number.focus();
                }
                if(personName.length === 0){
                    name.focus();
                }
            } else{
                closebg.fadeIn();
                frame.fadeIn();
                $('.personal-close').fadeIn();
                $('.personalized-jersey').animate({'right': '0'}, 100);
                $('.personalized-jersey').css('z-index', 0);
                $("#ejersey-trigger").trigger('click');
                $.ajax({
                    type: "GET",
                    url: "/_jersey/generate",
                    data: {
                        name: personName,
                        number: personNumber
                    },
                    success: function(res) {
                        if(res == 'error'){
                            name.val('');
                            number.val('');
                            img.attr("src", "/uniforms/base.png");
                            img.fadeIn(500);
                            $('.error').show();
                        }
                        else{
                            img.attr("src", "/_jersey/view/" +res);
                            img.fadeIn(1000);
                            $('.personal-target p').hide();
                            $("#download-share").attr("href", url+ "_jersey/download/" +res);
                            $("#fb-share").attr("href", "http://www.facebook.com/sharer.php?u=" +url+ "_jersey/view/" +res);
                            $("#twt-share").attr("href", "http://twitter.com/share?text=Check%20out%20my%20personalized%20@DallasStars%20jersey!%20Create%20your%20own%20here:&url=" +url+ "_jersey/view/" +res);
                            $("#email-share").attr("href", "mailto:?subject=Check Out My Personalized Dallas Stars Jersey Photo&body=" + url+ "_jersey/view/" +res);
                            $('.error').hide();
                        }
                    }
                });
            }
        });

        $('.error').click(function() {
            $('.error').hide();
        });
        img.click(function() {
            $('.error').hide();
        });
        closebg.click(function () {
            $('.error').hide();
            $('.personal-close').hide();
            closebg.fadeOut();
            frame.fadeOut();
            img.fadeOut();
        });
        $('.personal-close').click(function () {
            $('.error').hide();
            $('.personal-close').hide();
            closebg.fadeOut();
            frame.fadeOut();
            img.fadeOut();
        });
        $('.personal-share *').click(function(e) {
            e.stopPropagation();
        });
        $('.personal-share').click(function() {
            $('.error').hide();
            if ($('.personal-share').css('right') < '0') {
                $('.personal-share').animate({'right': '0'}, 200);
            } else {
                $('.personal-share').animate({'right': '-386'}, 200);
            }
        });
        $('.personal-download').click(function() {
            var dl = $('#person-img').attr("src");
            dl = dl.replace('view','download');
            window.open(dl);
        });
        shareLaunch.click(function() {
            var shareNameVal = shareName.val();
            var shareNumberVal = shareNumber.val();
            if(shareNumber == ''){
                shareNumber.focus();
            }
            if(shareName == ''){
                shareName.focus();
            }
            else{
                img.fadeOut();
                $.ajax({
                    type: "GET",
                    url: "/_jersey/generate",
                    data: {
                        name: shareNameVal,
                        number: shareNumberVal
                    },
                    success: function(res) {
                        if(res == 'error'){
                            shareName.val('');
                            shareNumber.val('');
                            img.attr("src", "/uniforms/base.png");
                            img.fadeIn(500);
                            $('.error').show();
                        }
                        else{
                            img.attr("src", "/_jersey/view/" +res);
                            $('.personal-share').animate({'right': '-386'}, 200);
                            img.fadeIn(1000);
                            $('.personal-target p').hide();
                            $("#download-share").attr("href", url+ "_jersey/download/" +res);
                            $("#fb-share").attr("href", "http://www.facebook.com/sharer.php?u=" +url+ "_jersey/view/" +res);
                            $("#twt-share").attr("href", "http://twitter.com/share?text=Check%20out%20my%20personalized%20@Stars%20jersey!%20Create%20your%20own%20here:&url=" +url+ "_jersey/view/" +res);
                            $("#email-share").attr("href", "mailto:?subject=Check Out My Personalized Dallas Stars Jersey Photo&body=" + url+ "_jersey/view/" +res);
                            $('.error').hide();
                            img.fadeIn();
                        }
                    }
                });
            }
        });
    }());

    /************ Popups ************/
    (function(){
        var popupContain = $('.popup-contain');
        var popupItem = $('.popup');
        var hasPop = $('.haspop');
        var popupItems = $('#wrapper').find(popupItem);
        var checkVideo;
        var checkSound = 0;
        hasPop.each(function(){
            $(this).click(function(){
                popupContain.fadeIn();
                $('.renew-catch').fadeIn();
                popupItem.hide();
                $('.' + $(this).data('popup')).show();
                $(window).trigger('resize');
                checkVideo = $(this).hasClass('isvideo');
                if(checkVideo){
                    checkSound = sound;
                    removeSound();
                    currentlyPlaying = $(this).data('popup');
                    try {
                        document.createEvent("TouchEvent");
                    } catch (e) {
                        setTimeout(function() {
                            jwplayer(currentlyPlaying).play(true).seek('0');
                        }, 800);
                    }
                }
            });
        });
        $('.close-pop, .renew-close').click(function(){
            popupContain.fadeOut();
            $('#renew-complete').fadeOut();
            $('.renew-catch').fadeOut();
        });
        $('.buy-button').click(function() {
            $('.renew-contain').fadeIn();
        });
    }());

    $('.invoice-click').click(function() {
        if ( history.pushState ) {
            history.pushState( null, document.title, window.location+'#' );
        }
    });

    /************ Content ************/
    $(".sub-page-nav-item").click(function(){
        var self = $(this)
            , parentPage = self.closest(".page")
            , currentPageStyle = parentPage.data("current")
            , activePage = $('.nav-item.active').data("page") + "-page" || "page"
            , activeSubPage = self.data("page")
        ;
        if(ga){
            ga('send', 'pageview', { 'page': location.pathname + location.search  + '#' + activePage + '_' + activeSubPage});
        }
        parentPage.removeClass(currentPageStyle).addClass(activeSubPage).data("current", activeSubPage);
        $("#"+currentPageStyle).hide();
        $("#"+activeSubPage).show();

    });


    $('.submit-cover').click(function() {
        var extraDetails = '';
        $('.extra-details').html('');
        $('.renew-button').hide();
        $('.playoff-extra').hide();
        if (($('input[name=payment]:checked', '#renewForm').val()) && ($('input[name=playoff]:checked', '#renewForm').val())) {
            var transChoice = ($('input[name=payment]:checked', '#renewForm').val());
            var playoffChoice = ($('input[name=playoff]:checked', '#renewForm').val());

            var transTitle = $('.'+transChoice+'_title').html();
            $('.transaction-title').html(transTitle);
            var transDetails = $('.'+transChoice+'_desc').html();
            $('.transaction-details').html(transDetails);
            var playoffDetails = $('.'+playoffChoice+'_desc').html();
            $('.playoff-details').html(playoffDetails);


            if ($('input[name=playoffExtra]:checked', '#renewForm').size() > 0){
                extraDetails += 'I am interested in additional Playoff seats for the entire playoff!';
            }

            if ($('input[name=fullExtra]:checked', '#renewForm').size() > 0){
                if (extraDetails.length) {
                    extraDetails += '<br>';
                }
                extraDetails += 'I am interested in additional seats for the 2014-15 season!';
            }

            if (extraDetails) {
                $('.extra-details').html(extraDetails);
                $('.playoff-extra').show();
            }

            $('#paymentInfo').val(($('.'+transChoice+'_title').text())+($('.'+transChoice+'_desc').text()));
            $('#playoffInfo').val($('.'+playoffChoice+'_desc').text());

            $('.renew-backdrop').css({'z-index': '9900'});
            $('#renew-confirm').fadeIn();
        } else if ($('input[name=payment]:checked', '#renewForm').val()) {
            alert('You have not selected a Stanley Cup Playoff Option. Please select an item from Step 2 to proceed.');
        } else {
            alert('You have not selected a Payment Plan. Please select an item from Step 1 to proceed.');
        }
    });

    $('.renew-submit').click(function() {
        $.post('/_renewal/', $("#renewForm").serialize(), function(data){});
        $('#renew-confirm').hide();
        $('#renew').fadeOut();
        $('.renew-button').hide();
        $('html, body').scrollTop(10);
        $('#renew-complete').fadeIn();
    });
    $('.renew-back').click(function() {
        $('.renew-backdrop').animate({'z-index': '9600'}, 100);
        $('.renew-button').show();
        $('#renew-confirm').fadeOut();
    });
    $('.renew-complete-back').click(function() {
        window.location.href = '#';
        window.location.reload();
    });
// Confirm appt time

// variable to hold request
    var request;
// bind to the submit event of our form
    $("#confirm-appt-form").submit(function(event){
        var $form = $(this);
        var $inputs = $form.find("input, select, button, textarea");
        var serializedData = $form.serialize();

        $inputs.prop("disabled", true);

        request = $.ajax({
            url: "/_confirm_appt",
            type: "post",
            data: serializedData
        });

        request.done(function (response, textStatus, jqXHR){
            $('.confirm-thanks').fadeIn(function(){
                $('.acquisition-btn').hide();
                $('.renew-catch').delay(3000).fadeOut();
                $('.popup-contain').delay(3000).fadeOut();
            });
        });

        request.fail(function (jqXHR, textStatus, errorThrown){
            console.error(
                "The following error occurred: "+
                    textStatus, errorThrown
            );
        });

        request.always(function () {
            $inputs.prop("disabled", false);
        });

        event.preventDefault();
    });

});
