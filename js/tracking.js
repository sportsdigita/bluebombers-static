$(document).ready(function () {
    $('.nav-item .message').click(function() {
        _gaq.push(['_trackEvent', 'Tabs', 'Click', 'A Message from Jim Lites']);
    });
    $('.nav-item .renewal').click(function() {
        st('Perks');
        _gaq.push(['_trackEvent', 'Tabs', 'Click', 'Renewal Benefits']);
    });
    $('.nav-item .season').click(function() {
        st('Programs');
        _gaq.push(['_trackEvent', 'Tabs', 'Click', 'Season Ticket Pricing']);
    });
    $('.nav-item .playoff').click(function() {
        st('Playoffs');
        _gaq.push(['_trackEvent', 'Tabs', 'Click', 'Playoff Pricing']);
    });
    $('.nav-item .seatmap').click(function() {
        st('Pricing');
        _gaq.push(['_trackEvent', 'Tabs', 'Click', '3D Seat Map']);
    });
    $('.message1').click(function() {
        _gaq.push(['_trackEvent', 'Videos', 'Watch', 'A Message from Jim Lites']);
    });
    $('.message2').click(function() {
        _gaq.push(['_trackEvent', 'Videos', 'Watch', 'What Season Ticket Holders Mean']);
    });
    $('.message3').click(function() {
        _gaq.push(['_trackEvent', 'Videos', 'Watch', 'Reasons for Optimism']);
    });
    $('.message4').click(function() {
        _gaq.push(['_trackEvent', 'Videos', 'Watch', 'Improved Game Experience']);
    });
    $('.message5').click(function() {
        _gaq.push(['_trackEvent', 'Videos', 'Watch', 'What to Expect in the Future']);
    });
    $('.message6').click(function() {
        _gaq.push(['_trackEvent', 'Videos', 'Watch', 'Thank You']);
    });
});
