$(document).ready(function() {
    function getTimeUntil(date) {
        var diff = new Date(date).getTime() - new Date().getTime();
        var cd = 24 * 60 * 60 * 1000,
            ch = 60 * 60 * 1000;
        var days = '' + Math.floor (diff / cd);
        var hours = '0' + Math.floor( (diff - days * cd) / ch);
        var minutes = '0' + Math.floor( (diff - days * cd - hours * ch) / 60000);
        var seconds = '0' + Math.floor( (diff - days * cd - hours * ch - minutes * 60000) / 1000);
        return {
            "days": days,
            "hours": hours.slice(-2),
            "minutes": minutes.slice(-2),
            "seconds": seconds.slice(-2)
        };
    }
    var diff = getTimeUntil("December 12, 2014 17:00:00");
    $(".countdown3 .days").html(diff.days);
    $(".countdown3 .hours").html(diff.hours);
    $(".countdown3 .minutes").html(diff.minutes);
    $(".countdown3 .seconds").html(diff.seconds);

    var seconds;
    setInterval(function() {
        var diff = getTimeUntil("December 12, 2014 17:00:00");
        $(".countdown3 .days").html(diff.days);
        $(".countdown3 .hours").html(diff.hours);
        $(".countdown3 .minutes").html(diff.minutes);
        $(".countdown3 .seconds").html(diff.seconds);
    }, 10);

});